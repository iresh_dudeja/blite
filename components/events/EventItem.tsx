/* eslint-disable react-native/no-inline-styles */
import React, {useState, ReactNode} from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  Switch,
  Image as ReactImage,
  StyleSheet,
} from 'react-native';
import {I18n} from '@aws-amplify/core';
import Icon from 'react-native-vector-icons/Ionicons';
import {COLORS} from '../../src/constants/colors';
import Collapsible from 'react-native-collapsible';

type showTypes =
  | 'arrowOnly'
  | 'arrowAndText'
  | 'arrowAndImage'
  | 'switchOnly'
  | 'none';

export interface EventItemProps {
  name: string;
  show: showTypes;
  selectedText?: string;
  selectedImage?: string;
  switchEnabled?: boolean;
  toggleSwitchEnabled?: any;
  onPress?(): void;
  marginLeft?: number;
  color?: string;
  flipArrow?: boolean;
  item: ReactNode;
}

export const EventItem = ({
  name,
  show,
  selectedText,
  selectedImage,
  switchEnabled,
  toggleSwitchEnabled,
  onPress,
  marginLeft,
  color,
  flipArrow,
  item,
}: EventItemProps) => {
  const [isEventItemCollapsed, setIsEventItemCollapsed] = useState(true);

  return (
    <View
      style={{...styles.container, marginLeft: marginLeft ? marginLeft : 10}}>
      <TouchableOpacity
        key={name}
        onPress={() => {
          setIsEventItemCollapsed(tPrev => !tPrev);
        }}
        disabled={show === 'switchOnly'}>
        <View style={styles.item}>
          <Text
            style={{
              ...styles.itemText,
              fontWeight: '600',
              color: color ? color : COLORS.black,
            }}>
            {I18n.get(name)}
          </Text>

          {show === 'arrowOnly' && (
            <Icon
              name={flipArrow ? 'arrow-down' : 'arrow-forward'}
              color={color ? color : COLORS.black}
              size={25}
            />
          )}

          {show === 'arrowAndText' && (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              {<Text style={styles.selectedText}>{selectedText}</Text>}
              <Icon
                name={flipArrow ? 'arrow-down' : 'arrow-forward'}
                color={color ? color : COLORS.black}
                size={25}
              />
            </View>
          )}

          {show === 'arrowAndImage' && (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              {
                <ReactImage
                  style={{width: 30, height: 20}}
                  source={{uri: selectedImage}}
                  resizeMode={'contain'}
                />
              }
              <Icon
                name={flipArrow ? 'arrow-down' : 'arrow-forward'}
                color={color ? color : COLORS.black}
                size={25}
              />
            </View>
          )}

          {show === 'switchOnly' && (
            <Switch
              trackColor={{false: 'lightgrey', true: COLORS.mainCombiStyle1}}
              thumbColor={COLORS.accent1}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitchEnabled}
              value={switchEnabled}
            />
          )}
        </View>
      </TouchableOpacity>
      <Collapsible collapsed={isEventItemCollapsed}>{item}</Collapsible>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginRight: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.accent6,
  },

  item: {
    flexDirection: 'row',
    marginVertical: 10,
    marginHorizontal: 10,
    justifyContent: 'space-between',
  },

  itemText: {
    color: COLORS.black,
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 26,
  },

  selectedText: {
    fontWeight: '300',
    fontSize: 16,
    marginRight: 5,
  },
});
