/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, View, Pressable, StyleSheet} from 'react-native';
import {COLORS} from '../../src/constants/colors';
interface EventDetailsProps {
  index: number;
  title: string;
  description: string;
  onPress: any;
}

export const EventDetails = ({
  index,
  title,
  description,
  onPress,
}: EventDetailsProps) => {
  return (
    <View style={{margin: 10, backgroundColor: COLORS.accent5}}>
      <View style={{padding: 10}}>
        <Text
          style={{
            fontSize: 17,
            fontWeight: '600',
            alignSelf: 'center',
            marginBottom: 5,
          }}>
          {title}
        </Text>
        <Text style={{fontSize: 14, alignSelf: 'center', marginVertical: 10}}>
          {description}
        </Text>
        <Pressable
          style={({pressed}) => [
            {
              backgroundColor: pressed
                ? 'rgb(210, 230, 255)'
                : COLORS.mainCombiStyle1,
            },
            styles.button,
          ]}
          onPress={onPress}>
          <Text
            style={{
              color: COLORS.accent1,
              fontWeight: '500',
              fontSize: 18,
            }}>{`Pay 10 \u20AC`}</Text>
        </Pressable>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    height: 40,
    borderRadius: 5,
  },
});
