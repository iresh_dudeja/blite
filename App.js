/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';

import Amplify from 'aws-amplify';
import config from './aws-exports';
import {withAuthenticator} from 'aws-amplify-react-native';
import {HomeScreen} from './src/events/HomeScreen';
import {CLIENT} from './src/apollo/client';
import {COLORS} from './src/constants/colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Auth from '@aws-amplify/auth';
import Purchases from 'react-native-purchases';
Amplify.configure(config);

const Stack = createStackNavigator();

const App = () => {
  useEffect(() => {
    Purchases.setDebugLogsEnabled(true);
    Purchases.setup('MQWptVUkrtuLhwxgnVQytYulvXpDPWCs');
  }, []);
  return (
    <ApolloProvider client={CLIENT}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
              title: 'Events',
              headerStyle: {
                backgroundColor: COLORS.accent4,
              },
              headerTitleStyle: {
                fontWeight: 'bold',
              },
              headerRight: () => (
                <AntDesign.Button
                  name="logout"
                  size={22}
                  color={COLORS.black}
                  backgroundColor={COLORS.accent4}
                  onPress={() => {
                    Auth.signOut();
                  }}
                />
              ),
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </ApolloProvider>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  backgroundStyle: {
    backgroundColor: 'gray',
  },
});

export default withAuthenticator(App);
