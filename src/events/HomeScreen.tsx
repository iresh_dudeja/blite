/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useLayoutEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {useLazyQuery} from '@apollo/client';
import {EVENTS_QUERY} from '../queries/events.query';
import {EventItem} from '../../components/events/EventItem';
import {COLORS} from '../constants/colors';
import {EventDetails} from '../../components/events/EventDetails';
import Purchases, {PURCHASE_TYPE} from 'react-native-purchases';

export const HomeScreen = () => {
  const eventQueryParameters = {
    attributes: ['coverPicture', 'displayName', 'description'],
    tags: [],
    skip: 0,
    take: 10,
    searchTerm: '',
    startedFilter: 'NOT_HELD',
  };

  const [getEvents, {data, loading, error, fetchMore}] = useLazyQuery(
    EVENTS_QUERY,
    {
      variables: eventQueryParameters,
      fetchPolicy: 'cache-and-network',
    },
  );

  const [eventsListState, setEventsListState] = useState([]);

  const [refreshing, setRefreshing] = React.useState(false);

  const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getEvents();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  useLayoutEffect(() => {}, []);

  const getPackages = async () => {
    try {
      const offerings = await Purchases.getOfferings();
      if (
        offerings.current !== null &&
        offerings.current.availablePackages.length !== 0
      ) {
        console.log(
          `HomeScreen | Current Offering packages: ${JSON.stringify(
            offerings.current.availablePackages,
            null,
            4,
          )}`,
        );
      }
    } catch (e) {
      Alert.alert('Error getting offers', e.message);
    }
  };

  useEffect(() => {
    // Get current available packages
    // getPackages();
  }, []);

  const getProducts = async () => {
    try {
      const tProducts = await Purchases.getProducts(
        ['com.blite.sandboxapp.event.10'],
        PURCHASE_TYPE.INAPP,
      );

      if (tProducts !== null && tProducts.length !== 0) {
        console.log(
          `HomeScreen | tProducts: ${JSON.stringify(tProducts, null, 4)}`,
        );
      }
    } catch (e) {
      Alert.alert('Error getting tProducts', e.message);
    }
  };

  const purchaseProduct = async () => {
    try {
      const tProduct = await Purchases.purchaseProduct(
        'com.blite.sandboxapp.event.10',
        null,
        PURCHASE_TYPE.INAPP,
      );

      if (tProduct !== null) {
        console.log(
          `HomeScreen | tProduct: ${JSON.stringify(tProduct, null, 4)}`,
        );
      }
    } catch (e) {
      Alert.alert('Error purchase tProduct', e.message);
    }
  };

  useEffect(() => {
    // getProducts();
  }, []);

  useEffect(() => {
    getEvents();
  }, []);

  // useEffect(() => {
  //   console.log(
  //     `HomeScreen  | Events: ${JSON.stringify(eventsListState, null, 4)}`,
  //   );
  // }, [data]);

  // Handle events response from server
  useEffect(() => {
    if (data) {
      const {events} = data.events!;
      let updatedEvents = [...events];
      if (updatedEvents !== null) {
        updatedEvents.forEach((tEvent, index) => {
          const eventAttributes = {};
          const eventLocationAttributes = {};
          let genres: string = '';
          const {attributes, location, tags} = tEvent;

          if (attributes) {
            attributes!.forEach(tAttribute => {
              switch (tAttribute!.type) {
                case 'string':
                  eventAttributes[tAttribute!.name!] = tAttribute!.stringValue;
                  break;
              }
            });
          }

          if (location) {
            const {attributes} = location;
            if (attributes) {
              attributes!.forEach(tAttribute => {
                switch (tAttribute!.type) {
                  case 'string':
                    eventLocationAttributes[tAttribute!.name!] =
                      tAttribute!.stringValue;
                    break;
                }
              });
            }
          }

          if (tags) {
            if (tags.length > 2) {
              genres = `${tags[0]!.name!}, ${tags[1]!.name}... `;
            } else {
              genres = tags.map(t => t!.name).join(', ');
            }
          }

          tEvent = {
            ...tEvent,
            eventAttributes,
            eventLocationAttributes,
            genres,
          };
          updatedEvents[index] = tEvent;
        });
      }
      setEventsListState(updatedEvents);
    }
  }, [data]);

  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size={'large'} color={COLORS.gray} />
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.screen}>
        <Text>{`${error.message}`}</Text>
      </View>
    );
  }

  interface SingleEventProps {
    index: number;
    eventId: string;
    title: string;
    description: string;
  }

  const SingleEvent = ({
    index,
    eventId,
    title,
    description,
  }: SingleEventProps) => {
    return (
      <EventItem
        name={title}
        selectedText={''}
        show={'arrowAndText'}
        flipArrow={false}
        onPress={() => {}}
        item={
          <EventDetails
            index={index}
            title={title}
            description={description}
            onPress={async () => {
              console.log(`Event id ${eventId} is pressed`);
              // await getProducts();
              // await getPackages();
              await purchaseProduct();
            }}
          />
        }
      />
    );
  };

  return (
    <SafeAreaView style={styles.screen}>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {eventsListState.map((tItem, index) => {
          return (
            <SingleEvent
              eventId={tItem.id}
              index={index}
              title={tItem.eventAttributes.displayName}
              description={tItem.eventAttributes.description}
            />
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  screen: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.accent1,
  },

  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});
