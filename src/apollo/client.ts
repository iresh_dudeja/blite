import ApolloLinkTimeout from 'apollo-link-timeout';
import {
  ApolloClient,
  ApolloLink,
  createHttpLink,
  from,
  split,
} from '@apollo/client';
import {cache} from './cache';
import {setContext} from '@apollo/client/link/context';
import {onError} from '@apollo/client/link/error';
import Auth from '@aws-amplify/auth';

const SERVER_URL = 'https://test-app-server.boombazoo.com/graphql';
const timeoutLink = new ApolloLinkTimeout(30000); // 30 second timeout

const httpLink = createHttpLink({
  uri: SERVER_URL,
  useGETForQueries: true,
});

const customURILink = new ApolloLink((operation, forward) => {
  operation.setContext({uri: `${SERVER_URL}/${operation.operationName}`});
  return forward(operation);
});

const getNewToken = async () => {
  let newToken = '';
  await Auth.currentSession()
    .then((res) => {
      const tAccessToken = res.getAccessToken();
      const tJwtToken = tAccessToken.getJwtToken();
      // console.log(`App.js | Refreshed Token is: ${tJwtToken}`);
      newToken = tJwtToken;
    })
    .catch((err) => {
      throw err;
    });
  return newToken;
};


export const authLink = setContext(async (_, {headers}) => {
  const token = await getNewToken();
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
      localeid: 'en',
    },
  };
});

const errorLink = onError(({graphQLErrors, networkError, operation}) => {
  console.log(
    `client.ts | [Error]:  OperationName: ${JSON.stringify(
      operation.operationName,
    )}`,
  );

  if (graphQLErrors) {
    graphQLErrors.forEach(({message, locations, path, extensions}) => {
      console.log(
        `client.ts | [GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
          locations,
          null,
          4,
        )}, Path: ${path}, Extension: ${JSON.stringify(extensions, null, 4)}`,
      );

      if (message.includes('no valid auth')) {
      }
    });
  }

  if (networkError) {
    console.log(`client.ts | [Network error]: ${networkError}`);
  }
});



export const CLIENT = new ApolloClient({
  // link: authLink.concat(httpLink),
  link: from([
    // timeStartLink,
    // logTimeLink,
    timeoutLink,
    errorLink,
    authLink,
    customURILink.concat(httpLink),
  ]),
  cache: cache,
});
