// EventsScreen queries
import {gql} from '@apollo/client';

export const EVENTS_QUERY = gql`
  query events(
    $attributes: [String]
    $tags: [String]
    $skip: Float
    $take: Float
    $searchTerm: String
    $startedFilter: StartedFilter
  ) {
    events(
      tags: $tags
      skip: $skip
      take: $take
      searchTerm: $searchTerm
      startedFilter: $startedFilter
    ) {
      events {
        id
        name
        startDate
        endDate
        link
        visible
        location {
          position
          attributes(names: $attributes) {
            type
            name
            stringValue
          }
        }
        attributes(names: $attributes) {
          type
          name
          stringValue
        }
        tags {
          name
        }
      }
      totalCount
    }
  }
`;
