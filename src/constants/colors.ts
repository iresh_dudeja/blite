export const COLORS = {
  primary: '#b44666',
  black: '#000',
  darkGrey: '#7e7e7e',
  lightGrey: '#E9E9E9',
  accent1: '#fff',
  accent2: '#5059ae',
  accent3: '#adb4bc',
  accent4: '#f5f5f5',
  accent5: '#eee',
  accent6: '#dddddd',
  gray: 'gray',
  //possible color combinations
  //style one:
  mainCombiStyle1: '#BF0436',
  accentCombiStyle2: '#085973',
  accentCombiStyle3: '#BF0436',
  //style two:
  mainCombiStyle4: '#8C0327',
  accentCombiStyle5: '#0E354',
  accentCombiStyle6: '#BF0436',
  //style three:
  mainCombiStyle7: '#BF0436',
  accentCombiStyle8: '#085973',
  accentCombiStyle9: '#FF1F5A',

  //colors from App design
  main: '#458296',
};
