/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const axios = require('axios');
const gql = require('graphql-tag');
const graphql = require('graphql');
const {print} = graphql;

const POOL_SERVER_TEST_URL =
  'https://test-app-server.boombazoo.com:8443/graphql';

// We want this lambda to run in VPC for security reasons. Check VPC option for lamda in AWS lambda console
const POOL_SERVER_VPC_URL = 'http://172.31.33.115:4001/graphql';

const createUser = gql`
  mutation createUser(
    $username: String
    $firstname: String
    $lastname: String
    $email: String
    $roles: [String]
    $phonenumber: String
    $birthDate: DateInput
    $gender: GenderType
  ) {
    createUser(
      username: $username
      firstname: $firstname
      lastname: $lastname
      email: $email
      roles: $roles
      phonenumber: $phonenumber
      birthDate: $birthDate
      gender: $gender
    ) {
      id
      username
      firstname
      lastname
      roles
      email
      active
    }
  }
`;

const isRunningLocally = !process.env.AWS_EXECUTION_ENV;
console.log('IsLocal: ', isRunningLocally);

exports.handler = async (event, context, callback) => {
  try {
    const graphqlData = await axios({
      url: isRunningLocally ? POOL_SERVER_TEST_URL : POOL_SERVER_VPC_URL,
      method: 'post',
      headers: {
        localeid:
          'locale' in event.request.userAttributes
            ? event.request.userAttributes.locale
            : 'de',
      },
      data: {
        query: print(createUser),
        variables: {
          username: event.userName,
          firstname:
            'name' in event.request.userAttributes
              ? event.request.userAttributes.name
              : '',
          lastname:
            'family_name' in event.request.userAttributes
              ? event.request.userAttributes.family_name
              : '',
          email:
            'email' in event.request.userAttributes
              ? event.request.userAttributes.email
              : '',
          roles: ['REGULAR'],
          phonenumber:
            'phone_number' in event.request.userAttributes
              ? event.request.userAttributes.phone_number
              : '',
          birthDate:
            'birthdate' in event.request.userAttributes
              ? {mode: 'TEXT', text: event.request.userAttributes.birthdate}
              : {mode: 'TEXT', text: '1800-01-01'},
          gender:
            'gender' in event.request.userAttributes
              ? event.request.userAttributes.gender
              : '',
        },
      },
    });
    const response = {
      graphqlData: JSON.stringify(graphqlData.data),
    };
    console.log(response);
    // Return to Amazon Cognito
    callback(null, event);
  } catch (err) {
    console.log('error creating user: ', err);
    callback(null, event);
  }
};
